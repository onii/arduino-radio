// My own basic radio

#include <RDA5807M.h>
#include <avr/sleep.h>
#include <EEPROM.h>
#include <EnableInterrupt.h> // Lazy way for 328p
// https://github.com/GreyGnome/EnableInterrupt

RDA5807M radio;

volatile byte btnPushed = 0;
int currFreq = 0;
int preset1 = 0;
int preset2 = 0;
int preset3 = 0;

void setup() {
  // D2 - D6 buttons
  pinMode(2, INPUT_PULLUP);
  pinMode(3, INPUT_PULLUP);
  pinMode(4, INPUT_PULLUP);
  pinMode(5, INPUT_PULLUP);
  pinMode(6, INPUT_PULLUP);

  // D7 & D8 LEDs
  pinMode(7, OUTPUT);
  pinMode(8, OUTPUT);

  // Turn on power LED
  digitalWrite(7, HIGH);

  // Turn on indicator LED
  digitalWrite(8, HIGH);
  initRadio();
  digitalWrite(8, LOW);

}

void loop() {
  // If we pushed a button
  if (btnPushed != 0) {

    // Detach interrupts
    disableInterrupt(2);
    disableInterrupt(3);
    disableInterrupt(4);
    disableInterrupt(5);
    disableInterrupt(6);

    // Turn on indicator LED
    digitalWrite(8, HIGH);

    // Determine button press duration
    byte btnCount = 0;
    do {
      btnCount++;
      delay(100);
    } while (digitalRead((btnPushed + 1)) == LOW);

    if (btnCount > 30) {
      // Long press

      switch (btnPushed) {
        case 1: // Preset 1
          preset1 = radio.getFrequency();
          EEPROM.put(2, preset1);
          break;
        case 2: // Preset 2
          preset2 = radio.getFrequency();
          EEPROM.put(4, preset2);
          break;
        case 3: // Preset 3
          preset3 = radio.getFrequency();
          EEPROM.put(6, preset3);
          break;
        case 4: // Prev button
          blinkFreq();
          break;
        case 5: // Next button
          break;
      }
    } else {
      // Short press

      switch (btnPushed) {
        case 1: // Preset 1
          radio.setFrequency(preset1);
          currFreq = preset1;
          EEPROM.put(0, currFreq);
          break;
        case 2: // Preset 2
          radio.setFrequency(preset2);
          currFreq = preset2;
          EEPROM.put(0, currFreq);
          break;
        case 3: // Preset 3
          radio.setFrequency(preset3);
          currFreq = preset3;
          EEPROM.put(0, currFreq);
          break;
        case 4: // Prev
          radio.seekDown();
          // Wait until done seeking
          radioSeeking();
          EEPROM.put(0, currFreq);
          break;
        case 5: // Next
          radio.seekUp();
          // Wait until done seeking
          radioSeeking();
          EEPROM.put(0, currFreq);
          break;
      }
    }

    digitalWrite(8, LOW); // Turn off indicator LED
    btnPushed = 0;

  }
  // Nothing left to do

  // Attach interrupts
  enableInterrupt(2, wakeUp, CHANGE);
  enableInterrupt(3, wakeUp, CHANGE);
  enableInterrupt(4, wakeUp, CHANGE);
  enableInterrupt(5, wakeUp, CHANGE);
  enableInterrupt(6, wakeUp, CHANGE);

  // Can't do anything here, so we'll sleep
  set_sleep_mode(SLEEP_MODE_PWR_DOWN);
  sleep_mode();
}

void wakeUp() {
  // Figure out which button was pressed
  if (digitalRead(2) == LOW) btnPushed = 1;
  if (digitalRead(3) == LOW) btnPushed = 2;
  if (digitalRead(4) == LOW) btnPushed = 3;
  if (digitalRead(5) == LOW) btnPushed = 4;
  if (digitalRead(6) == LOW) btnPushed = 5;

}

void initRadio() {
  // Turn on radio
  radio.begin(RDA5807M_BAND_WEST);

  // Force mono audio
  radio.updateRegister(RDA5807M_REG_CONFIG, RDA5807M_FLG_MONO, 0x1);

  // Read EEPROM
  EEPROM.get(0, currFreq);
  EEPROM.get(2, preset1);
  EEPROM.get(4, preset2);
  EEPROM.get(6, preset3);

  // If EEPROM is blank
  if (currFreq == -1) {
    currFreq = 9090; // 90.9MHz
    EEPROM.put(0, currFreq);
  }
  if (preset1 == -1) {
    preset1 = 9090; // 90.9MHz
    EEPROM.put(2, preset1);
  }
  if (preset2 == -1) {
    preset2 = 10730; // 107.3MHz
    EEPROM.put(4, preset2);
  }
  if (preset3 == -1) {
    preset3 = 9810; // 98.1MHz
    EEPROM.put(6, preset3);
  }

  // Set freq
  delay(1000); // Necessary for stability
  radio.setFrequency(currFreq);
}

void radioSeeking() {
  // Delay until done seeking
  bool doneSeeking = 0;
  int newFreq = 0;
  while (doneSeeking == 0) {
    currFreq = radio.getFrequency();
    delay(1000);
    newFreq = radio.getFrequency();

    if (currFreq == newFreq) {
      doneSeeking = 1;
    }
  }
}

void blinkFreq() {

  currFreq = radio.getFrequency();
  digitalWrite(8, LOW);

  // First digit
  int i = 0;
  delay(2000);
  while (i < (currFreq / 1000)) {
    digitalWrite(8, HIGH);
    delay(1000);
    digitalWrite(8, LOW);
    delay(500);
    i++;
  } delay(2000);

  // Second digit
  i = 0;
  while (i < ((currFreq / 100) % 10)) {
    digitalWrite(8, HIGH);
    delay(1000);
    digitalWrite(8, LOW);
    delay(500);
    i++;
  } delay(2000);

  // Third digit
  i = 0;
  while (i < ((currFreq / 10) % 10)) {
    digitalWrite(8, HIGH);
    delay(1000);
    digitalWrite(8, LOW);
    delay(500);
    i++;
  }
}
