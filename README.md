# Arduino radio

My personal radio based on Arduino and RDA5807M.

5 buttons using internal pull-ups on D2 - D6. 
First 3 are presets / press & hold to store current station in EEPROM.
Last two are forward seek / backward seek.
All button presses store current frequency in EEPROM to resume next time.
Debounce on everything.

Voltage dividing potentiometer before speaker output to adjust volume.

Activity light for feedback on button presses.

Uses the [RDA5807M library](https://github.com/csdexter/RDA5807M).
